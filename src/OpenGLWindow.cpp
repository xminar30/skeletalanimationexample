#include <geSG/Animation.h>
#include <geSG/AnimationChannel.h>
#include <geSG/AnimationManager.h>
#include <geSG/SkeletalAnimationMixer.h>
#include <geSG/Skeleton.h>

#include <ste/DAG.h>

#include <AssimpModelLoader.h>

#include <OpenGLWindow.h>

#include <QtGui/QOpenGLContext>
#include <geGL/geGL.h>
#include <geUtil/Text.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <QDebug>

#include <iostream>
#include <vector>

class Bone {
private:
	std::vector<std::shared_ptr<Bone>> children{};
	std::shared_ptr<glm::mat4> transform{std::make_shared<glm::mat4>(1)};

	glm::mat4 offset{1};
	glm::mat4 matrix{1};

	void updateBones(glm::mat4 const& m)
	{
		matrix = m * offset * *transform;
		for (auto bone : children) {
			bone->updateBones(matrix);
		}
	}

public:
	Bone()
	{}

	Bone(glm::mat4 offset) : offset{offset}
	{}

	void addChild(std::shared_ptr<Bone> child)
	{
		this->children.push_back(child);
	}

	void updateSkeleton()
	{
		matrix = *transform;
		for (auto bone : children) {
			bone->updateBones(matrix);
		}
	}

	glm::mat4 const& getMatrix() const
	{
		return matrix;
	}

	std::shared_ptr<glm::mat4> getTransformPtr() const
	{
		return this->transform;
	}
};

class Skeleton {
private:
	std::vector<std::shared_ptr<Bone>> bones{};
	std::shared_ptr<Bone> root{std::make_shared<Bone>()};

public:
	Skeleton()
	{
		bones.push_back(root);
	}

	std::shared_ptr<Bone> getRootBone()
	{
		return root;
	}

	std::shared_ptr<Bone> addBone(std::shared_ptr<Bone> parent, glm::mat4 offset)
	{
		std::shared_ptr<Bone> new_bone = std::make_shared<Bone>(offset);

		bones.push_back(new_bone);
		parent->addChild(new_bone);

		return new_bone;
	}

	void update()
	{
		root->updateSkeleton();
	}

	std::vector<std::shared_ptr<Bone>> const& getBones()
	{
		return bones;
	}
};

using namespace std::chrono_literals;

const float CUBE_SIZE = 4.f;
std::vector<float> ge::examples::OpenGLWindow::trianglePos = {
	-.5f*CUBE_SIZE, -.5f*CUBE_SIZE, -.5f*CUBE_SIZE,
	-.5f*CUBE_SIZE, -.5f*CUBE_SIZE, 0.5f*CUBE_SIZE,
	-.5f*CUBE_SIZE, 0.5f*CUBE_SIZE, -.5f*CUBE_SIZE,
	-.5f*CUBE_SIZE, 0.5f*CUBE_SIZE, 0.5f*CUBE_SIZE,
	0.5f*CUBE_SIZE, -.5f*CUBE_SIZE, -.5f*CUBE_SIZE,
	0.5f*CUBE_SIZE, -.5f*CUBE_SIZE, 0.5f*CUBE_SIZE,
	0.5f*CUBE_SIZE, 0.5f*CUBE_SIZE, -.5f*CUBE_SIZE,
	0.5f*CUBE_SIZE, 0.5f*CUBE_SIZE, 0.5f*CUBE_SIZE,
};

std::vector<unsigned> ge::examples::OpenGLWindow::indices = {
	0,1,2,
	1,3,2,
	2,7,3,
	2,6,7,
	2,0,4,
	2,4,6,
	3,5,1,
	3,7,5,
	0,5,1,
	0,4,5,
	4,5,7,
	4,7,6,
};

//std::shared_ptr<SkinnedMesh> testModel;

ge::examples::OpenGLWindow::OpenGLWindow(QWindow *parent)
	: QWindow(parent)
	, initialized(false)
	, context(nullptr)
	, manager(new ge::sg::AnimationManager)
{
	setSurfaceType(QWindow::OpenGLSurface); //this needs to be set otherwise makeCurrent and other gl context related functions will fail
	surfaceFormat.setVersion(4, 5);
	surfaceFormat.setProfile(QSurfaceFormat::CoreProfile);
}

ge::examples::OpenGLWindow::~OpenGLWindow()
{
}

Skeleton skeleton{};

/*
void loadImages(ge::sg::Scene* scene, std::string& imageDir)
{
   std::shared_ptr<ge::sg::DefaultImage> defaultImage(make_shared<ge::sg::DefaultImage>());

   for(auto model : scene->models)
   {
      for(std::shared_ptr<ge::sg::Material> material : model->materials)
      {
         for(std::vector<std::shared_ptr<ge::sg::MaterialComponent>>::iterator it = material->materialComponents.begin(); it != material->materialComponents.end(); ++it)
         {
            if((*it)->getType() == ge::sg::MaterialComponent::ComponentType::IMAGE)
            {
               ge::sg::MaterialImageComponent *img = dynamic_cast<ge::sg::MaterialImageComponent*>((*it).get());
               //cout << img->semantic << " " << img->filePath << endl;
               std::string textFile(imageDir + img->filePath);
               std::shared_ptr<QtImage> image(QtImageLoader::loadImage(textFile.c_str()));
               if(image == nullptr)
               {
                  cout << "  " << "FAILED TO LOAD!" << "substituting default image\n";
                  img->image = defaultImage;
               }
               else {
                  img->image = image;
               }
            }
         }
      }
   }
}
*/

std::shared_ptr<ge::sg::Scene> my_scene;


/**
 * Create OpenGL context with Qt with appropriate surface format.
 * Then initialize geGL and creating geGL context wrapper with OpenGL
 * functions entry points. Also prints out the GL_VERSION string.
 */
void ge::examples::OpenGLWindow::initialize()
{
	if (initialized) return;

	if (!context)
	{
		context = new QOpenGLContext(this);
		context->setFormat(surfaceFormat);
		bool success = context->create();
		if (!success)
		{
			std::cerr << "ERROR: Cannot create OpenGL context" << std::endl;
		}
	}

	//let's say to the OS that we want to work with this context
	context->makeCurrent(this);

	ge::gl::init();
	gl = std::make_shared<ge::gl::Context>();

//	testModel = std::make_shared<SkinnedMesh>();
//	testModel->LoadMesh(MODEL_FOLDER "boblampclean.md5mesh");
//	std::cout << MODEL_FOLDER "WYVERN.FBX" << std::endl;

	std::shared_ptr<ge::gl::Shader> vertexShader = std::make_shared<ge::gl::Shader>(GL_VERTEX_SHADER, ge::util::loadTextFile(VERTEX_SHADER));
	std::shared_ptr<ge::gl::Shader> fragmentShader = std::make_shared<ge::gl::Shader>(GL_FRAGMENT_SHADER, ge::util::loadTextFile(FRAGMENT_SHADER));
	shaderProgram = std::make_shared<ge::gl::Program>(vertexShader, fragmentShader);

	positions = std::make_shared<ge::gl::Buffer>(trianglePos.size() * sizeof(float), trianglePos.data());
	element = std::make_shared<ge::gl::Buffer>(indices.size() * sizeof(unsigned), indices.data());


	glm::mat4 perpective = glm::perspective(glm::radians(90.f), (float)width() / height(), 0.1f, 1000.f); // no resizing yet


	std::shared_ptr<Bone> first = skeleton.getRootBone();
	std::shared_ptr<Bone> second = skeleton.addBone(first, glm::translate(glm::mat4{1}, glm::vec3{1.1, 0, 0}));
	std::shared_ptr<Bone> third = skeleton.addBone(second, glm::translate(glm::mat4{1}, glm::vec3{1.1, 0, 0}));

	shaderProgram->setMatrix4fv("projection", glm::value_ptr(perpective));

	printError();

	VAO = std::make_shared<ge::gl::VertexArray>();

	VAO->bind();
	VAO->addElementBuffer(element);
	VAO->addAttrib(positions, 0, 3, GL_FLOAT);
	VAO->unbind();

	std::shared_ptr<ge::sg::MovementAnimationChannel> mvch = std::make_shared<ge::sg::MovementAnimationChannel>(); //movement channel
	std::shared_ptr<ge::sg::MovementAnimationChannel> mvch2 = std::make_shared<ge::sg::MovementAnimationChannel>(); //movement channel

	std::shared_ptr<ge::sg::Animation> animation = std::make_shared<ge::sg::Animation>(); //animation
//	std::shared_ptr<ge::sg::Animation> animation2 = std::make_shared<ge::sg::Animation>(); //animation

	mvch->setTarget(first->getTransformPtr()); //set target the channel is manipulating
	animation->channels.push_back(mvch); // connects movement channel to this animation - one channel can be shared with multiple animations

	glm::vec3 tr1(2, 0, -10), tr0(-3, 0, -10); //translations for two key frames

	mvch->positionKF.emplace_back(0s, tr0); // translation goes from tr0 to tr1 in one sec
	mvch->positionKF.emplace_back(10.0s, tr1);

	glm::vec3 ax(1, 1, 0);
	ax = glm::normalize(ax);

	mvch->orientationKF.emplace_back(0s, glm::angleAxis(glm::radians(0.f), ax));
	mvch->orientationKF.emplace_back(2.5s, glm::angleAxis(glm::radians(90.f), ax));
	mvch->orientationKF.emplace_back(5s, glm::angleAxis(glm::radians(180.f), ax));
	mvch->orientationKF.emplace_back(7.5s, glm::angleAxis(glm::radians(270.f), ax));
	mvch->orientationKF.emplace_back(10s, glm::angleAxis(glm::radians(0.f), ax));

	animation->mode = ge::sg::Animation::Mode::LOOP;


	mvch2->setTarget(second->getTransformPtr()); //set target the channel is manipulating
//	animation2->channels.push_back(mvch2); // connects movement channel to this animation - one channel can be shared with multiple animations
	animation->channels.push_back(mvch2);


	mvch2->orientationKF.emplace_back(0s, glm::angleAxis(0.f, glm::vec3(1, 0, 0)));
	mvch2->orientationKF.emplace_back(2.5s, glm::angleAxis(glm::radians(90.f), glm::vec3(1, 0, 0)));
	mvch2->orientationKF.emplace_back(5s, glm::angleAxis(glm::radians(50.f), glm::vec3(0, 1, 0)) * glm::angleAxis(glm::radians(180.f), glm::vec3(1, 0, 0)));
	mvch2->orientationKF.emplace_back(7.5s, glm::angleAxis(glm::radians(270.f), glm::vec3(1, 0, 0)));
	mvch2->orientationKF.emplace_back(10s, glm::angleAxis(glm::radians(-50.f), glm::vec3(0, 1, 0)) * glm::angleAxis(0.f, glm::vec3(1, 0, 0)));

//	mvch2->positionKF.emplace_back(0s, glm::vec3(1.1f, 0, 0));

//	animation2->mode = ge::sg::Animation::Mode::LOOP;

	manager->playAnimation(animation, ge::core::time_point::clock::now());
//	manager->playAnimation(animation2, ge::core::time_point::clock::now());

//	*third->getTransformPtr() = glm::translate(*third->getTransformPtr(), glm::vec3(1.1, 0, 0));
	*third->getTransformPtr() = glm::rotate(*third->getTransformPtr(), glm::radians(45.f), glm::vec3(1, 0, 0));

	gl->glEnable(GL_DEPTH_TEST);

    my_scene.reset(AssimpModelLoader::loadScene(MODEL_FOLDER"bob/boblampclean.md5mesh"));

    my_scene->rigModels.back()->animationMixer->playAnimation(0, ge::core::time_point::clock::now());
    my_scene->rigModels.push_back(std::shared_ptr<ge::sg::RigModelInstance>(AssimpModelLoader::createRigModelInstance(my_scene->rigModels.front()->rigModel)));
    my_scene->rigModels.back()->animationMixer->playAnimation(0, ge::core::time_point::clock::now() + 1s);

	initialized = true;
}

void ge::examples::OpenGLWindow::render()
{
	const qreal retinaScale = devicePixelRatio();
	gl->glViewport(0, 0, width() * retinaScale, height() * retinaScale);
	gl->glClearColor(.392, .584, 0.929, 1.0);
	gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	shaderProgram->use();

//	animation->update(ge::core::time_point::clock::now());
	manager->update(ge::core::time_point::clock::now());

	VAO->bind();

	skeleton.update();

    /*
	for (auto bone : skeleton.getBones()) {
		shaderProgram->setMatrix4fv("model", glm::value_ptr(bone->getMatrix()));
		gl->glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
	}
    */

    my_scene->rigModels.front()->animationMixer->update(ge::core::time_point::clock::now());
    my_scene->rigModels.back()->animationMixer->update(ge::core::time_point::clock::now());

    shaderProgram->setMatrix4fv("view", glm::value_ptr(glm::rotate(glm::translate(glm::mat4(1), glm::vec3(0, -25, -100)), glm::radians(-90.f), glm::vec3(1,0,0))));
    shaderProgram->setMatrix4fv("pos", glm::value_ptr(glm::rotate(glm::translate(glm::mat4(1), glm::vec3(-40, 0, 0)), glm::radians(0.f), glm::vec3(1,0,0))));
	for (auto bone : my_scene->rigModels.front()->skeletonInstance->bones) {
		shaderProgram->setMatrix4fv("model", glm::value_ptr(glm::translate(glm::scale(bone.transform->getMatrix(), glm::vec3(1)), glm::vec3(0,0,0))));
		gl->glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
	}

    shaderProgram->setMatrix4fv("pos", glm::value_ptr(glm::rotate(glm::translate(glm::mat4(1), glm::vec3(40, 0, 0)), glm::radians(0.f), glm::vec3(1,0,0))));
	for (auto bone : my_scene->rigModels.back()->skeletonInstance->bones) {
		shaderProgram->setMatrix4fv("model", glm::value_ptr(glm::translate(glm::scale(bone.transform->getMatrix(), glm::vec3(1)), glm::vec3(0,0,0))));
		gl->glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, nullptr);
	}

//    my_scene->rootNode->

//	shaderProgram->setMatrix4fv("model", glm::value_ptr(glm::scale(glm::translate(glm::mat4(1), glm::vec3(0, 0, -4)), glm::vec3(0.01))));
//	testModel->Render();

	printError();

	context->swapBuffers(this);
}

void ge::examples::OpenGLWindow::printError() const
{
	auto err = this->gl->glGetError();
	if (err != GL_NO_ERROR)
	{

		std::cout << err << std::endl;
	}
}

void ge::examples::OpenGLWindow::renderNow()
{
	if (!isExposed()) return;
	if (!initialized) initialize();

	context->makeCurrent(this);
	//context never got released so no need to make it current again

	render(); //do the simple rendering

	requestUpdate();
	//release context only if necessary
}

bool ge::examples::OpenGLWindow::event(QEvent *event)
{
	switch (event->type())
	{
	case QEvent::UpdateRequest:
		renderNow();
		return true;
	default:
		return QWindow::event(event);
	}
}

void ge::examples::OpenGLWindow::exposeEvent(QExposeEvent *event)
{
	if (isExposed())
	{
		renderNow();
	}
}