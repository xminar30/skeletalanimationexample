#include <SimplegeSGRenderer.h>

#include <QGuiApplication>
#include <AssimpModelLoader.h>
#include <assimp/postprocess.h>
#include <QtImageLoader.h>

#include <geSG/SkeletalAnimationMixer.h>
#include <geSG/DefaultImage.h>

#include <ste/DAG.h>

#include <QFileInfo>

using namespace std;

/**
 * Simple function that loads images that are found at image components of materials
 * and populate its image property.
 * 
 * \warning Assumes relative texture file paths or empty imageDir. There should be an extended check
 *          in production code. 
 */
void loadImages(ge::sg::Scene* scene, std::string& imageDir)
{
   if (!scene) return;

   std::shared_ptr<ge::sg::DefaultImage> defaultImage(make_shared<ge::sg::DefaultImage>());

   for(auto model: scene->models)
   {
      for(std::shared_ptr<ge::sg::Material> material : model->materials)
      {
         for(std::vector<std::shared_ptr<ge::sg::MaterialComponent>>::iterator it = material->materialComponents.begin(); it != material->materialComponents.end(); ++it)
         {
            if((*it)->getType() == ge::sg::MaterialComponent::ComponentType::IMAGE)
            {
               ge::sg::MaterialImageComponent *img = dynamic_cast<ge::sg::MaterialImageComponent*>((*it).get());
               //cout << img->semantic << " " << img->filePath << endl;
               std::string textFile(imageDir + img->filePath.substr(0,img->filePath.length() - 4));
               std::shared_ptr<QtImage> image(QtImageLoader::loadImage(textFile.c_str()));
               if(image == nullptr)
               {
                  cout << "  " << "FAILED TO LOAD!" << "substituting default image\n";
                  img->image = defaultImage;
               }
               else {
                  img->image = image;
               }
            }
         }
      }
   }
}

int main(int argc, char** argv)
{
   QGuiApplication app(argc, argv);

   QQuickWindow qw;
   qw.resize(800, 600);
   fsg::SimplegeSGRenderer basicRenderer(&qw);


   // model file from https://www.katsbits.com/download/models/md5-example.php
   QString modelFileName(APP_RESOURCES"/models/bob/boblampclean.md5mesh");

   QFileInfo fi(modelFileName);
   string modelPath(qUtf8Printable(fi.canonicalPath() + "/"));

   std::shared_ptr<ge::sg::Scene> scene;
   scene.reset(AssimpModelLoader::loadScene(modelFileName.toUtf8().constData(), aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_GenSmoothNormals | aiProcess_FlipUVs));
   if(!scene)
   {
      cerr << "Failed to load scene!" << endl;      
      return 1;
   }
   basicRenderer.setScene(scene);

   if (scene && !scene->rigModels.empty()) {
      std::shared_ptr<ge::sg::MatrixTransformNode> orig = scene->rootNode;
      scene->rootNode = std::make_shared<ge::sg::MatrixTransformNode>();
      scene->rootNode->data = std::make_shared<ge::sg::MatrixTransform>(glm::mat4(1.0f));
      scene->rigModels.push_back(std::make_shared<ge::sg::RigModelInstance>(scene->rigModels.back().get()));
      *orig->data->getRefMatrix() = glm::translate(glm::mat4(1.0f), glm::vec3(-30, 0, 0));
      scene->rootNode->children.push_back(std::make_shared<ge::sg::MatrixTransformNode>());
      scene->rootNode->children.back()->data = std::make_shared<ge::sg::MatrixTransform>(glm::translate(glm::mat4(1.0f), glm::vec3(30, 0, 0)));
      scene->rigModels.back()->node = scene->rootNode->children.back();
      scene->rootNode->children.push_back(orig);
      cout << "animations count: " << scene->rigModels.front()->animationMixer->animations.size() << endl;

      scene->rigModels.front()->animationMixer->playAnimation(0, ge::core::time_point::clock::now());
      scene->rigModels.back()->animationMixer->playAnimation(0, ge::core::time_point::clock::now() + 1s);
   }
   else {
      cout << "Model doesn't have bones" << endl;
   }

   //load Images
   loadImages(scene.get(), modelPath);


   qw.show();

   return app.exec();
}