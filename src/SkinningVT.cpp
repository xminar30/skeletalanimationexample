#include <SkinningVT.h>

#include <glsg/GLScene.h>
#include <geSG/Material.h>
#include <geSG/Scene.h>
#include <geSG/Skeleton.h>
#include <geGL/VertexArray.h>
#include <geGL/Texture.h>
#include <glsg/EnumToGL.h>
#include <geGL/geGL.h>

#include <ste/DAG.h>

#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace fsg;
using namespace ge::gl;
using namespace ge::sg;
using namespace ge::glsg;

/**
 * Sets the scene to visualize.
 * \param scene GLScene to visualize.
 */
void fsg::SkinningVT::setScene(std::shared_ptr<ge::glsg::GLScene>& scene)
{
   glscene = scene;
}

/**
 * Creates VAOs, preps uniforms and textures that the VT needs for visualization.
 */
void fsg::SkinningVT::processScene()
{
   std::queue<ge::sg::MatrixTransformNode*> nodes;
   std::list<std::shared_ptr<ge::sg::RigModelInstance>> rigModelInstances;

   for (auto m : glscene->scene->rigModels) {
      if (m->node)
         rigModelInstances.push_back(m);
   }

   nodes.push(glscene->scene->rootNode.get());

   while (!nodes.empty()) {
      ge::sg::MatrixTransformNode* node = nodes.front(); nodes.pop();
      for (auto child : node->children) {
         nodes.push(child.get());
      }
      for (auto m = rigModelInstances.begin(); m != rigModelInstances.end();) {
         if ((*m)->node.get() == node) {
            this->rigModelInstanceMap[node].push_back(*m);
            m = rigModelInstances.erase(m);
         }
         else
            ++m;
      }
   }

   for(auto& meshIt: glscene->GLMeshes)
   {
      //mesh geometry
      shared_ptr<VertexArray> VAO = make_shared<VertexArray>(gl->getFunctionTable());
      for(auto& glattrib: meshIt.second)
      {
         if(glattrib.attributeDescriptor->semantic == AttributeDescriptor::Semantic::indices)
         {
            VAO->addElementBuffer(glattrib.BO);
         }
         else
         {
            int attribLocation = semantic2Attribute(glattrib.attributeDescriptor->semantic);
            if(attribLocation != -1)
            {
               if (glattrib.attributeDescriptor->semantic == AttributeDescriptor::Semantic::boneIDs)
                  VAO->addAttrib(glattrib.BO, attribLocation, glattrib.attributeDescriptor->numComponents, translateEnum(glattrib.attributeDescriptor->type), (GLsizei)glattrib.attributeDescriptor->stride, 0, false, 0U, VertexArray::AttribPointerType::I);
               else
                  VAO->addAttrib(glattrib.BO, attribLocation, glattrib.attributeDescriptor->numComponents, translateEnum(glattrib.attributeDescriptor->type), (GLsizei)glattrib.attributeDescriptor->stride);
            }
         }
      }
      VAOContainer[meshIt.first] = VAO;

      //material
      Material * mat = meshIt.first->material.get();

      //one way to get the component you want but the more frequently used way is bellow
      auto component = mat->getComponent<MaterialSimpleComponent>(MaterialSimpleComponent::Semantic::diffuseColor);
      if(component)
      {
         unsigned sizeinbytes = component->size * component->getSize(component->dataType);
         colorContainer[mat] = make_unique<unsigned char[]>(sizeinbytes);
         //memcpy(colorContainer[mat].get(), component->data.get(), sizeinbytes); //this is the same as below but less general
         std::copy_n(component->data.get(), sizeinbytes, colorContainer[mat].get());
      }

      for(auto& comp: mat->materialComponents)
      {
         if(comp->getType() == MaterialComponent::ComponentType::IMAGE)
         {
            auto imageComponent = static_cast<MaterialImageComponent*>(comp.get());
            diffuseTextureConatiner[meshIt.first] = glscene->textures[imageComponent];
         }
      }
   }
} 

/**
 * Currently does nothing.
 */
void SkinningVT::drawSetup()
{
}

void SkinningVT::drawNode(ge::sg::MatrixTransformNode* node, glm::mat4 transform)
{
   transform = node->data->getMatrix() * transform;

   if (rigModelInstanceMap.find(node) != rigModelInstanceMap.end()) {
      for (auto rigModel : rigModelInstanceMap[node]) {
         program->setMatrix4fv("bones", rigModel->skeletonInstance->getBoneTransforms(), rigModel->skeletonInstance->bones.size());
         program->setMatrix4fv("model", glm::value_ptr(transform));

         for (auto& meshIt : glscene->GLMeshes)
         {
            Mesh* mesh = meshIt.first;

            Texture *texture = diffuseTextureConatiner[meshIt.first].get();
            texture->bind(0);

            VertexArray * VAO = VAOContainer[mesh].get();
            VAO->bind();
            gl->glDrawElements(translateEnum(mesh->primitive), mesh->count, translateEnum(mesh->getAttribute(AttributeDescriptor::Semantic::indices)->type), 0);
            VAO->unbind();
         }
      }
   }

   for (auto n : node->children)
   {
      drawNode(n.get(), transform);
   }
}

/**
 * Use provided shader and draws the provided scene.
 */
void SkinningVT::draw()
{
   program->use();
   if(!glscene) return;

   drawNode(glscene->scene->rootNode.get(), glscene->scene->rootNode->data->getMatrix());

}

/**
 * Internal helper function that returns attribute position for given semantic or -1
 * if the attrib is not to be used by this VT.
 */
int SkinningVT::semantic2Attribute(AttributeDescriptor::Semantic semantic)
{
   switch (semantic)
   {
      case AttributeDescriptor::Semantic::position: return 0;
      case AttributeDescriptor::Semantic::normal: return 1;
      case AttributeDescriptor::Semantic::texcoord: return 2;
      case AttributeDescriptor::Semantic::boneIDs: return 3;
      case AttributeDescriptor::Semantic::boneWeights: return 4;
      default: return -1;
   }
}
