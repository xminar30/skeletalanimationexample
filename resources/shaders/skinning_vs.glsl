#version 430

layout(location=0) in vec3 pos;
layout(location=1) in vec3 normal_in;
layout(location = 2) in vec2 texcoords;
layout(location = 3) in uvec4 boneIDs;
layout(location = 4) in vec4 boneWeights;

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform mat4 bones[200];

out vec2 uv;
out vec3 normal;
out vec3 fragPos;

void main(){
   mat4 bt = bones[boneIDs[0]] * boneWeights[0];
   bt     += bones[boneIDs[1]] * boneWeights[1];
   bt     += bones[boneIDs[2]] * boneWeights[2];
   bt     += bones[boneIDs[3]] * boneWeights[3];

   vec4 newPos = bt * vec4(pos,1.0);
   vec3 newNormal = mat3(bt) * normal_in;

   normal = newNormal.xyz;
   uv = texcoords;
   gl_Position = projection * view * model * newPos;
   fragPos = vec3(model * newPos);
}
