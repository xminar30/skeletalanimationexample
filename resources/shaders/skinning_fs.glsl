#version 430

layout(location=0)out vec4 fragColor;

uniform layout(binding = 0) sampler2D diffTex;

uniform mat4 view;

in vec2 uv;
in vec3 normal;
in vec3 fragPos;

void main()
{
   vec3 norm = normalize(normal);
   vec3 lightDir = normalize(vec3(0,0,-100)-fragPos);
   float diff = max(dot(norm, lightDir), 0.0);
   fragColor = diff * texture(diffTex,uv);
}
